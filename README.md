# Data Extraction Requirements

## Project Goal

The goal of this project is to extract reward data from two JS-rendered websites which provide data for the Polkadot blockchain. These websites produce data on a continuous basis. The data extraction system should be set up to scrape these websites at regular intervals.

## Target websites

* Subscan: https://polkadot.subscan.io/
* Polkascan: https://polkascan.io/polkadot

## Entities

Here's a list of terminology that will help provide some context around the data that is being extracted.

* Sessions
  * A unit of time, sessions occur every 4 hours, so they are 6 sessions every day
* Eras
  * A unit of time, an Era last 24 hours, there are 6 sessions every Era
* Validators
  * Actors in the system that create blocks in the blockchain, they earn rewards for their work
* Nominators
  * Actors in the system that delegate their tokens to Validators, when delegating the tokens are locked. Nominators earn rewarads for delegating their tokens
* Extrinsics
  * Stores metadata found in a blockchain transaction, contains many events
* Events
  * Represents meaningful occurrences that have

## Technololgy

* Python / Scrapy
* Any technology capable of extracting data from a JavaScript rendered website (Splash, Selenium, etc)
* I plan on using the following scraping services from ScrapingHub
  * Scraping Cloud: https://www.scrapinghub.com/scrapy-cloud/
  * Crawlera: https://www.scrapinghub.com/crawlera/

## Requirements

* Build a continuous scraper that extracts reward data from subscan and polkascan.
* Data extracted will be stored in an AWS S3 bucket in JSON format
* Scraper should be set up in a way such that **only new data is extracted**. Previously crawled urls should be recorded and should not be crawled more than once unless the Admin has re-run a job while troubleshooting.  Middleware such as such as [deltafetch](https://support.scrapinghub.com/support/solutions/articles/22000200411-delta-fetch-addon) and [DotScrapy Persistence ](https://support.scrapinghub.com/support/solutions/articles/22000221912-incremental-crawls-with-scrapy-and-deltafetch-in-scrapy-cloud)may be used.
* The Admin should be able to monitor the scraper jobs to determine if an issue has occurred
* The Admin should have the ability to re-run a scraper job in the event of an issue

## Scraper Details

### Step 1: Extract Session Data from Polkascan (website #1)

* The spider should start by extracting data from a specific session (**Session Detail page**)

  * Session urls are constructed like the following: https://polkascan.io/polkadot/session/session/700
    * Sessions are sequential and occur once every 6 hours, or 4 times a day.
    * The scraper will need to keep track of previously crawled sessions in order to know which sessions it should crawl and how to construct the crawl urls (`https://polkascan.io/polkadot/session/session/701`, `https://polkascan.io/polkadot/session/session/702`, etc)


  ![](images/pr_step_1.png)

  **Extract the Session header data**

  * Once on the **Session Detail page**, the spider should collect the following session information:
    * session url
    * session number
    * start block
    * end block
    * era
    * blocks
    * number of validators


  * Navigate to the Validator Session Detail page
    * The spider should crawl the link of the "Details" button for each validator in the list
    * They will then be navigated to the **Validator Session Detail page**


**Extract the Validator session header data**

  ![](images/pr_step_2_v2.png)

  * While on the **Validator Session Detail page**, the following information should be collected for each validator

    * validator session url
    * validator address
    * stash account address
    * stash acccount name
    * controller account address
    * controller account name
    * bonded total amount
    * bonded own stash amount
    * additional bonded by nominators amount
    * number of nominators
    * commission fee percentage

**Extract the data for each Nominators data**

  * For each of the nominators in the table below, the following data should be collected:
    * nominator address
    * nominator name
    * nominator bonded amount

* The JSON data for each session should look like the following:


```
{
    "session_url": "https://polkascan.io/polkadot/session/session/700",
    "session_number": 700,
    "start_block": "1673887",
    "end_block": "1676278",
    "era": "110",
    "num_of_blocks": "2392",
    "num_of_validators": "197",
    "validators": [
      {
        "validator_address": "1FCu68ZwBHNzZLcGa92eHwvR61hk3MpjrhiqN96xF9vWS1Q",
        "validator_session_url": "https://polkascan.io/polkadot/session/validator/700-0",
        "stash_account_name": "1m1hm",
        "stash_account_address": "1FCu68ZwBHNzZLcGa92eHwvR61hk3MpjrhiqN96xF9vWS1Q",
        "controller_account_name": "1r3yC",
        "13gAjcC56upxCdaNS69jB7rvGDzimcztTm9Jxhfgma8w9jor",
        "total_bonded_amount": "3,726,313.2086226298 DOT",
        "bonded_own_stash_amount": "9.93604061 DOT",
        "bonded_by_nominators_amount": "3,726,303.27258202 DOT",
        "num_of_nominators": "5",
        "commission_fee": "100%",
        "nominators": [
          {
            "nominator_address": "13VEhQuZqAqjHs32Wcukn9mHYKFEnELXsPZYAcc1x8yXERpF",
            "nominator_name": "13VEhQuZqAqjHs32Wcukn9mHYKFEnELXsPZYAcc1x8yXERpF",
            "amount_bonded": "19.4236126 DOT"
          },
          {
            "nominator_address": "165DtcEsijMqsnQMQT3qhN2NeSNpAEgQbAnAvuhXhzSUVWy4",
            "nominator_name": "165DtcEsijMqsnQMQT3qhN2NeSNpAEgQbAnAvuhXhzSUVWy4",
            "amount_bonded": "150.7 DOT",
          },
          ...
        ]
      },
      ...
    ]
}

```

* After successfully scraping the `session_url` and all of the related data above, the URL should be persisted and the Spider should ignore if it encounters the URL again.

## Step 2: Extract Reward data from Subscan (website #2)

This step involves pulling reward data from Subscan. This information is also available on Polkascan but requires many more requests to collect the same data.

* The spider will need to access a list of validator addresses. This list should contain all of the unique validator addresses that have ever been identified in a crawled session. We can discuss where that list should live, I can make a JSON endpoint available that returns a list of unique validator addresses. This list would then be used to construct an array of start urls with each url structured like the following: https://polkadot.subscan.io/validator/1FCu68ZwBHNzZLcGa92eHwvR61hk3MpjrhiqN96xF9vWS1Q?tab=reward

* The URL leads to the **Validater Detail page** with "Reward&Slash" selected as the active tab
* The "Reward&Slash" tab contains a table that lists all of the rewards and slash events associated with this Validator.


  ![](images/pr_step_3.png)

* The spider should click on the "View All" button to navigate to the **Validator Rewards page**, a paginated list of all of the rewards and slashing events for the validator.


  ![](images/pr_step_4_v2.png)

* Each row in the table on the **Validator Rewards page** has an url listed under the "Extrinsic Hash" column. Each Extrinsic Hash URL is structured as the following:
  * https://polkadot.subscan.io/extrinsic/0xfea820a129ae923d5bb43070eb5338ac94e6f8c0c7b8f9d2a7be0432f3c6f05c
  * This URL will lead to a **Extrinsics Detail page** that lists all of the rewards and slash events for multiple validators and nominators.


  ![](images/pr_step_5.png)

  * The spider will crawl this URL and navigate to the **Extrinsics Detail page**.


  ![](images/pr_step_6.png)

  **Extract the Extrinsics header data**

  * While on the **Extrinsics Detail page**, the spider should capture the following data:
    * extrinsic url (the url of the page)
    * time
    * block
    * extrinsic hash
    * module type
    * call_type
    * sender address
    * fee_amount
    * nonce
    * result

**Extract the parameter data**

  * The Spider will click on the "View All" button in the Parameters section to expand the list

  * Then the Spider will collect the following information for each one of the parameters
    * parameter_number
    * validator_stash
    * era
    * call_function
    * call_index
    * call_module


  ![](images/pr_step_7.png)


**Extract the Events data**

  * For each of the events in the list, collect the following data:
    * event_id
    * docs
    * account_id
    * balance
    * action


  ![](images/pr_step_8_v2.png)

  * The JSON data for each extinsic should look like the following:


```
{
  "extrinsic_url": "https://polkadot.subscan.io/extrinsic/0xfea820a129ae923d5bb43070eb5338ac94e6f8c0c7b8f9d2a7be0432f3c6f05c",
  "time": "2020-09-17 12:29:54 (+UTC)",
  "block": "1633695",
  "extrinsic_hash": "0xfea820a129ae923d5bb43070eb5338ac94e6f8c0c7b8f9d2a7be0432f3c6f05c",
  "module_type": "Utility",
  "call_type": "Batch",
  "sender": "16VW8qAsdpQkGj3VeennjAYfRfL9kSQed2bLyiFWT7JHwkMh",
  "fee_amount": "0.16380001 DOT",
  "nonce": "69",
  "result": "Success",
  "parameters": [
    {
      "parameter_number": "0",
      "validator_stash": "14TFQiKmAXBqozphG4Njjhbic5Ggqt3eekFN7CAojyCALKEx",
      "era": "105",
      "call_function": "payout_stakers",
      "call_index": "0712",
      "call_module": "Staking"
    },
    ...
  ],
  "events": [
    {
      "event_id": "1633695-1",
      "action": "staking(Reward)",
      "docs" "staking(Reward)",
      "account_id": "14TFQiKmAXBqozphG4Njjhbic5Ggqt3eekFN7CAojyCALKEx",
      "balance": "1,433.2294415113"
    },
    {
      "event_id": "1633695-93",
      "docs": "Batch of dispatches completed fully with no error."
    },
    ...
  ]
}
```

* After successfully scraping the `extrinsic_url` and all of the related data above, the URL should be persisted and the Spider should ignore it when it encounters the URL again.

* The Spider should crawl each unique `extrinsic_url` found on the **Validator Detail page** (https://polkadot.subscan.io/validator/1FCu68ZwBHNzZLcGa92eHwvR61hk3MpjrhiqN96xF9vWS1Q?tab=reward) that has not previously been crawled and crawl the **Extrinsic Detail page** associated with each unique `extrinsic_url`

* The data extraction system will store the extracted JSON data in an AWS S3 bucket that I will provide.
